import os
from typing import Dict

import aiohttp
from bs4 import BeautifulSoup


class StravaWeb:
    """The helper class for the Strava Web client."""

    GET_LOGIN_URL = 'https://www.strava.com/login'
    POST_LOGIN_URL = 'https://www.strava.com/session'

    def __init__(self) -> None:
        """Init the API Helper."""
        self._session: aiohttp.ClientSession = aiohttp.ClientSession()

    async def _handle_form(self, get_url: str, post_url: str, form_id: str, data: Dict[str, str]) -> None:
        """Handle a strava form within the client."""
        async with self._session.get(get_url) as resp:
            text = await resp.text()
            soup = BeautifulSoup(text, 'html.parser')
            form = soup.find('form', id=form_id)
            authenticity_token = form.find('input', attrs={'name': 'authenticity_token'})['value']

        async with self._session.post(post_url, data={
            'authenticity_token': authenticity_token,
            **data,
        }) as resp:
            if resp.status != 200:
                raise ValueError(f'Error submitting the form {form_id} with code {resp.status}')

    async def login(self) -> None:
        """Login into strava."""
        await self._handle_form(
            get_url=self.GET_LOGIN_URL,
            post_url=self.POST_LOGIN_URL,
            form_id='login_form',
            data={
                'email': os.getenv('STRAVA_USERNAME', ''),
                'password': os.getenv('STRAVA_PASSWORD', ''),
            }
        )

    async def close(self) -> None:
        """Close the session."""
        await self._session.close()

    async def set_activity_visibility(self, activity_id: str, visibility: str = 'everyone') -> None:
        """Set an activity to public."""
        await self._handle_form(
            get_url=f'https://www.strava.com/activities/{activity_id}/edit',
            post_url=f'https://www.strava.com/activities/{activity_id}',
            form_id='edit-activity',
            data={
                '_method': 'patch',
                'activity[visibility]': visibility,
            }
        )
