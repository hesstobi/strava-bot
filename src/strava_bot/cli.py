import asyncio
import logging

import click
import click_log
from dotenv import load_dotenv

from strava_bot import bot

logger = logging.getLogger('gitlab_bot')
click_log.basic_config(logger)


CONTEXT_SETTINGS = {'help_option_names': ['-h', '--help']}


@click.group(context_settings=CONTEXT_SETTINGS, invoke_without_command=True, chain=True)
@click_log.simple_verbosity_option(logger)
def main() -> None:
    """Bot to do boring strava tasks."""
    load_dotenv()


@main.command('visibility', short_help='Set the visibility to public of activities')
def visibility() -> None:
    """Set the visibility of activities."""
    asyncio.run(bot.visibility())
