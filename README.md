# strava-bot 0.0.0

Bot to set activities public after a certain time.

Token
-----

Enter the url into your brouser and copy the returned code

    https://www.strava.com/oauth/authorize?client_id=42498&redirect_uri=http://localhost&response_type=code&approval_prompt=force&scope=activity:read_all,activity:write

Run then

curl -X POST https://www.strava.com/api/v3/oauth/token \
  -d client_id=42498 \
  -d client_secret=$CLIENT_SECRET \
  -d code=8bafc13e79b60226805936eddf3b011887da39d9 \
  -d grant_type=authorization_code


Pipeline
--------

Set up a git porject with this pipline configuration.

```yaml

stages:
  - schedule

workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - when: always

run:schedule:
  image: registry.gitlab.com/hesstobi/strava-bot:latest
  stage: schedule
  script:
    - strava_bot visibility
  dependencies: []
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'



```

Add this variables to your ci config:

* STRAVA_APPID=42498
* STRAVA_APPSECRET
* STRAVA_REFRESHTOKEN
* STRAVA_USERNAME
* STRAVA_PASSWORD

Create a schedule to run the bot.
