#!/bin/bash
set -eo pipefail
shopt -s nullglob

# logging functions
entry_log() {
	local type="$1"; shift
	printf '%s [%s] [Entrypoint]: %s\n' "$(date --rfc-3339=seconds)" "$type" "$*"
}
entry_note() {
	entry_log Note "$@"
}
entry_warn() {
	entry_log Warn "$@" >&2
}
entry_error() {
	entry_log ERROR "$@" >&2
	exit 1
}

# usage: file_env VAR [DEFAULT]
#    ie: file_env 'XYZ_DB_PASSWORD' 'example'
# (will allow for "$XYZ_DB_PASSWORD_FILE" to fill in the value of
#  "$XYZ_DB_PASSWORD" from a file, especially for Docker's secrets feature)
file_env() {
	local var="$1"
	local fileVar="${var}_FILE"
	local def="${2:-}"
	if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
		entry_error "Both $var and $fileVar are set (but are exclusive)"
	fi
	local val="$def"
	if [ "${!var:-}" ]; then
		val="${!var}"
	elif [ "${!fileVar:-}" ]; then
		val="$(< "${!fileVar}")"
	fi
	export "$var"="$val"
	unset "$fileVar"
}

# check arguments for an option that would cause app to stop
# return true if there is one
_want_help() {
	local arg
	for arg; do
		case "$arg" in
			-'?'|--help|--version)
				return 0
				;;
		esac
	done
	return 1
}

# check to see if this file is being run or sourced from another script
_is_sourced() {
	# https://unix.stackexchange.com/a/215279
	[ "${#FUNCNAME[@]}" -ge 2 ] \
		&& [ "${FUNCNAME[0]}" = '_is_sourced' ] \
		&& [ "${FUNCNAME[1]}" = 'source' ]
}


# Loads various settings that are used elsewhere in the script
# This should be called before any other functions
docker_setup_env() {
    # Define here your environment variables
    # e.g.
    # file_env 'DB_PASSWORD'
    :
}

# creates copy missing config files to the configdir
docker_create_configs() {
    # e.g
    # if [ ! -d "$CONFIGDIR" ]; then
    #    cp -R ./.defaults/configs $CONFIGDIR
    # fi
    :
}


_main() {
	# if command starts with an option or a subcommend, prepend the command
	commands=$(strava_bot --help)
    if [ "${1:0:1}" = '-' ] || [[ $commands == *"  $1  "* ]]; then
		set -- strava_bot "$@"
	fi

	# skip setup if they aren't running strava_bot or want an option that stops strava_bot
	if [ "$1" = 'strava_bot' ] && ! _want_help "$@"; then
		entry_note "Entrypoint script for strava_bot started."

		# Load various environment variables
		docker_setup_env "$@"

        # Init the default configurations files
        docker_create_configs

		# If container is started as root user, restart as dedicated user
		# if [ "$(id -u)" = "0" ]; then
		#	entry_note "Switching to dedicated user 'user'"
		#	exec gosu user "$BASH_SOURCE" "$@"
		# fi

	fi
	exec "$@"
}

# If we are sourced from elsewhere, don't perform any further actions
if ! _is_sourced; then
	_main "$@"
fi
