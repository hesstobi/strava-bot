from click.testing import CliRunner

from strava_bot.cli import main


def test_main():
    """Simple cli test."""
    runner = CliRunner()
    result = runner.invoke(main, [])
    assert result.exit_code == 0
