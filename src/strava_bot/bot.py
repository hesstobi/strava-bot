import logging

from strava_bot.api import StravaApi
from strava_bot.web import StravaWeb

logger = logging.getLogger(__name__)


async def visibility() -> None:
    """Set the visibility of activities."""
    web = StravaWeb()
    api = StravaApi()

    activities = [a['id'] async for a in api.activities(props_filter={'commute': False, 'private': True})]
    if activities:
        logger.warning('Found %s activities to set to public', len(activities))
        await web.login()

    for a in activities:
        await web.set_activity_visibility(activity_id=a)

    await web.close()
