import datetime
import os
from contextlib import asynccontextmanager
from typing import Any
from typing import AsyncGenerator
from typing import AsyncIterator
from typing import Dict
from typing import Optional

import aiohttp


class StravaApi:
    """Helper to access the api."""

    OAUTH_URL = 'https://www.strava.com/api/v3/oauth/token'
    ACTIVITIES_URL = 'https://www.strava.com/api/v3/athlete/activities'

    def __init__(self) -> None:
        """Init the API Helper."""
        self._access_token: Optional[str] = None
        self._access_token_expire: Optional[datetime.datetime] = None

    @asynccontextmanager
    async def access_token(self) -> AsyncIterator[str]:
        """Get the accesstoken."""
        if self._access_token is not None and \
            self._access_token_expire is not None and \
                self._access_token_expire > datetime.datetime.now():
            yield self._access_token
        else:
            async with aiohttp.ClientSession() as session:
                async with session.post(self.OAUTH_URL, data={
                    'client_id': os.getenv('STRAVA_APPID'),
                    'client_secret': os.getenv('STRAVA_APPSECRET'),
                    'refresh_token': os.getenv('STRAVA_REFRESHTOKEN'),
                    'grant_type': 'refresh_token',
                }) as resp:
                    v = await resp.json()
                    self._access_token_expire = datetime.datetime.fromtimestamp(v.get('expires_at', 0))
                    self._access_token = v.get('access_token')
            yield self._access_token  # type: ignore

    async def activities(
        self,
        before: datetime.timedelta = datetime.timedelta(weeks=4),
        after: datetime.timedelta = datetime.timedelta(weeks=12),
        props_filter: Dict[str, Any] = {}
    ) -> AsyncGenerator[Dict[str, Any], None]:
        """Get the activities."""
        async with self.access_token() as token:
            headers = {'Authorization': f'Bearer {token}'}
            async with aiohttp.ClientSession(headers=headers) as session:
                page = 1
                while True:
                    async with session.get(self.ACTIVITIES_URL, params={
                        'before': (datetime.datetime.now() - before).timestamp(),
                        'after': (datetime.datetime.now() - after).timestamp(),
                        'page': page,
                        'per_page': 100
                    }) as resp:
                        v = await resp.json()
                        if not v:
                            break
                        for a in v:
                            if all(a.get(k) == v for k, v in props_filter.items()):
                                yield a
                    page += 1
